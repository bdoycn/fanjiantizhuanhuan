"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs");
const configFilePath = path.resolve('./config.json');
let configText;
let config;
const CONFIG_NECESSARY_PARAMETERS = [
    'inputDir',
    'outputDir',
];
try {
    configText = fs.readFileSync(configFilePath, {
        encoding: 'utf-8',
    });
}
catch (ex) {
    throw new Error('未找到配置文件 config.json');
}
try {
    config = JSON.parse(configText);
}
catch (ex) {
    throw new Error('配置文件解析错误 config.json');
}
const configIsCorrect = CONFIG_NECESSARY_PARAMETERS.every(paramName => config[paramName]);
if (!configIsCorrect)
    throw new Error(`配置项错误, 配置必须包含: ${CONFIG_NECESSARY_PARAMETERS.join(',')}`);
exports.default = config;
;
//# sourceMappingURL=config.js.map