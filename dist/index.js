"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const config_1 = require("./config");
async function init() {
    // console.log('fs.opendir :>> ', fs);
    console.log('config.inputDir :>> ', config_1.default.inputDir);
    const rootDir = await fs.promises.opendir(config_1.default.inputDir);
    for await (const dirent of rootDir) {
        console.log(dirent.name);
    }
}
init();
//# sourceMappingURL=index.js.map