import * as path from 'path';
import * as fs from 'fs';

const configFilePath = path.resolve('./config.json');

let configText: string;
let config: IConfig;
const CONFIG_NECESSARY_PARAMETERS: string[] = [
  'inputDirPath',
  'outputDirPath',
  'fileExtensions',
  'ignoreDirsName',
  'ignoreFilesName',
  'translateRuleFilePath'
];

try {
  configText = fs.readFileSync(configFilePath, {
    encoding: 'utf-8',
  });
} catch (ex) {
  throw new Error('未找到配置文件 config.json');
}

try {
  config = JSON.parse(configText);
} catch (ex) {
  throw new Error('配置文件解析错误 config.json');
}

const configIsCorrect = CONFIG_NECESSARY_PARAMETERS.every(paramName => (config as any)[paramName]);
if (!configIsCorrect) throw new Error(`配置项错误, 配置必须包含: ${CONFIG_NECESSARY_PARAMETERS.join(',')}`);

export default config;


interface IConfig {
  inputDirPath: string,
  outputDirPath: string,
  fileExtensions: string[],
  ignoreDirsName: string[],
  ignoreFilesName: string[],
  translateRuleFilePath: string,
};
