import path = require("path");
import config from './config';

const { inputDirPath, outputDirPath } = config;

export function inputPathToOutputPath(
  inputPath: string,
  inputBasePath: string = inputDirPath,
  outputBasePath: string = outputDirPath,
): string {
  const relativePath = path.relative(inputBasePath, inputPath);
  const newPath = path.join(outputBasePath, relativePath);

  return newPath;
}
