import config from "./config";
import { OpenCC } from 'opencc';

const { translateRuleFilePath } = config;

const converter = new OpenCC(translateRuleFilePath);

function translate(content: string): Promise<string> {
  return converter.convertPromise(content);
}

export default translate;
